import React from 'react'
import $ from 'jquery';


const Playarea = () => (
  <div className="playarea">
  <svg viewBox="0 0 400 400">
  <rect className="orangecircle" x="10.6" y="50.1" width="67.5" height="60"/>
  <rect className="rectblue" x="10.6" y="150" width="67.5" height="60"/>
  </svg>
  <p> Click on the circle to animate it, and animate the rectangle after it.</p>
  </div>
)


export default Playarea
