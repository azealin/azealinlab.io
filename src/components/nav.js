import React from 'react'
import Scrollchor from 'react-scrollchor';
const Nav = () => (
  <div>
    <div className="nav">
      <Scrollchor to="#" className="nav-link">
        Home
      </Scrollchor>
      <Scrollchor to="#about" className="nav-link">
      Background
      </Scrollchor>
      <Scrollchor to="#skills" className="nav-link">

      Skills
      </Scrollchor>

      <Scrollchor to="#portfolio" className="nav-link">
      Portfolio
      </Scrollchor>

    </div>
  </div>
)

export default Nav
