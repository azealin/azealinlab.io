import React from 'react'
import Helmet from 'react-helmet'
import { ParallaxProvider } from 'react-scroll-parallax'

import { StickyContainer, Sticky } from 'react-sticky';
import { slide as Menu } from 'react-burger-menu'

import Scrollchor from 'react-scrollchor';

import Nav from './nav'

// import './index.scss'
import '../styles/main.scss'

import favicon16 from "../images/fav16x16.jpeg";
import favicon32 from "../images/fav32x32.jpeg";
import favicon64 from "../images/fav64x64.jpeg";

const Header = () => (
    <Sticky>
        {({
            style
          }) => (
            <div
            className="header"
            style={style}
            >
              <div className="header-container">
                <h1 style={{ margin: 0 }}>

                </h1>
                <Nav/>
              </div>
            </div>
          )}
    </Sticky>
)
const Footer = () => (
  <footer className="footer">
    <div className="footer-text">
    Contact me at : azealin@gmail.com, Site created in Gatsby, React, SCSS, Hosted on GitLab Pages<br/>
    Repo at : <a href="https://gitlab.com/azealin/azealin.gitlab.io/" target="_blank" rel="noopener noreferrer">https://gitlab.com/azealin/azealin.gitlab.io/</a>
    </div>
  </footer>
)

const TemplateWrapper = ({ children }) => (

  <ParallaxProvider>

    <StickyContainer>
    <div>
      <Helmet
        title="Ashley's Personal Site"
        meta={[
          { name: 'description', content: 'Personal Site for Ashley\'s Use' },
          { name: 'keywords', content: 'ashley yang' },
        ]}
        link={[
           { rel: "icon", type: "image/png", sizes: "16x16", href: `${favicon16}` },
           { rel: "icon", type: "image/png", sizes: "32x32", href: `${favicon32}` },
           { rel: "shortcut icon", type: "image/png", href: `${favicon64}` },
           { rel: "stylesheet", type:"text/css" , href:"https://fonts.googleapis.com/css?family=Maven+Pro|Righteous&display=swap"}

         ]}
         script= {
           [
             {"src" : "https://kit.fontawesome.com/6dae4ceaee.js"}
           ]
         }
      />
      <Sticky>
          {({
              style
            }) => (

          <div className="mobile-nav" style={style}>
          <Menu>

            <Scrollchor to="#" className="nav-link">
              Home
            </Scrollchor>
            <Scrollchor to="#about" className="nav-link">
            Background
            </Scrollchor>
            <Scrollchor to="#skills" className="nav-link">
            Skills
            </Scrollchor>
            <Scrollchor to="#portfolio" className="nav-link">
            Portfolio
            </Scrollchor>
          </Menu>
          </div>
        )}
      </Sticky>
      <Header />
      <div className="main-content-wrapper">
        {children}
      </div>
      <Footer />
    </div>

    </StickyContainer>
  </ParallaxProvider>
)

export default TemplateWrapper
