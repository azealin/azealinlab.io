import React from 'react'

const Portfolio = ({bgImage, name, href}) => (
  <a href={href} target="_blank" rel="noopener noreferrer" className="portfolio-link">
    <div
    className="portfolio-link-inner"
    style={{
      backgroundImage: `url(${bgImage})`,
      backgroundPosition: 'center',
      backgroundSize: '100% 100%',
      backgroundRepeat: 'no-repeat'
    }}
    >
    </div>
  </a>
)

export default Portfolio
