import React from 'react'

import Layout from '../components/layout'
import Portfolio from '../components/portfolio'
import Playarea from '../components/playarea'

import { Parallax } from 'react-scroll-parallax'

import dk from '../images/dk-img.png';
import imc from '../images/imc-img.png';
import sda from '../images/sda-img.png';
import dusa from '../images/dusa-img.png';
import bm from '../images/bm-img.png';
import sns from '../images/sns.png';

const copy = 'Ashley'.split('');
const copylast = 'Yang'.split('');

class IndexPage extends React.Component {
  	state = {
  		width: null
  	};

  	componentDidMount() {
    if (typeof window !== 'undefined') {
      this.handleWindowSizeChange();
  		window.addEventListener('resize', this.handleWindowSizeChange);
    }
  	}

  	// make sure to remove the listener
  	// when the component is not mounted anymore
  	componentWillUnmount() {

    if (typeof window !== 'undefined') {
  		window.removeEventListener('resize', this.handleWindowSizeChange);
    }
  	}
  	// dinamically detect window
  	handleWindowSizeChange = () => {
  		this.setState({ width: window.innerWidth });
  	};

  	disableParallax(windowWidth) {
  		return windowWidth <= 767;
  	}

  	render() {
  		return (
        <Layout>
          <div className="full-height-section">
              <Parallax disabled={this.disableParallax(this.state.width)}
                  x={[-300,100]}
                 className="top-left-abs-pos"
                >
             <h3 className="third-header">Developer</h3>
             </Parallax>
             <Parallax disabled={this.disableParallax(this.state.width)}
                 x={[-200,300]}
                className="bottom-right-abs-pos"
                slowerScrollRate
               >
            <h3 className="third-header">Bug Killer</h3>
            </Parallax>
            <Parallax disabled={this.disableParallax(this.state.width)}
                x={[-100,100]}
               className="bottom-left-abs-pos"
              >
           <h3 className="third-header">Dog Mom</h3>
           </Parallax>
             <div className="top-bar" />

             <span className="copy h1">
                 {copy.map((letter, i) => (
                     <Parallax disabled={this.disableParallax(this.state.width)}
                         key={`copy-${i}`}
                         x={[-50 * (i - 3),50 * (i - 3)]}
                         className={`copy-letter`}
                     >
                         {letter}
                     </Parallax>
                 ))}
             </span>
             <span className="copy copylast h1">
                 {copylast.map((letter, i) => (
                     <Parallax disabled={this.disableParallax(this.state.width)}
                         key={`copy-${i}`}
                         x={[-75 * (i - 2),75 * (i - 2)]}
                         className={`copy-letter`}
                     >
                         {letter}
                     </Parallax>
                 ))}
             </span>
             <div className="bottom-bar" />
         </div>
          <div className="full-height-section gray-bg" id="about">

              <Parallax disabled={this.disableParallax(this.state.width)} className=""
              y={[-25, 50]}
              >

                <div className="container-wrapper">
                  <h2 className="secondary-header">I am:</h2>
                  <p className="content">
                  I am a full-stack software engineer with 6 years industry experience specializing in front end web development involving HTML/CSS3, React (Javascript), Python, and PHP. I have extensive experience building custom React projects integrated with multiple APIs and hosting the stack with various AWS services including Cloudfront, Lambdas, and API Gateway. I also have experience creating custom themes in Wordpress and Webflow as well as managing custom landing pages and blogs integrated with Hubspot and Google Tag Manager/Google Analytics.
                  </p>
                  <h2 className="secondary-header">Education:</h2>
                  <p className="content">
                  B.S. in Computer Science - University of Arizona
                  </p>
                </div>
              </Parallax>

          </div>

          <div className="full-height-section" id="skills">
            <Parallax disabled={this.disableParallax(this.state.width)} className=""
                x={[-25, 30]}
              >

              <div className="container-wrapper">
                <h2 className="secondary-header">Proficient In:</h2>

                  <ul className="skills-list">
                    <li className="skills-list-li">HTML5/CSS3</li>
					<li className="skills-list-li">React</li>
                    <li className="skills-list-li">NodeJS</li>
                    <li className="skills-list-li">Javascript</li>
                    <li className="skills-list-li">JQuery</li>
                    <li className="skills-list-li">PHP (Laravel, Concrete5, Wordpress)</li>
                    <li className="skills-list-li">MYSQL/NoSQL/DynamoDB</li>
                    <li className="skills-list-li">AWS Lambda, AWS Api Gateway, AWS Cloudfront</li>
                    <li className="skills-list-li">Java</li>
                    <li className="skills-list-li">C</li>
                    <li className="skills-list-li">Python</li>
                    <li className="skills-list-li">GitHub, GitLab, Redmine, Trello, Bitbucket</li>
                  </ul>
              </div>
            </Parallax>

          </div>
          <div className="full-height-section gray-bg" id="portfolio">
            <h2 className="secondary-header">Portfolio: </h2>
            <h3 className="third-header">Work From DeferoUSA: </h3>

            <div className="portfolio-grid">
              <Portfolio bgImage={dk} name="DK Engineering" href="https://dkengin.com/"/>
              <Portfolio bgImage={imc} name="International Market Centers" href="https://www.imcenters.com/"/>
              <Portfolio bgImage={sda} name="Sun Devil Auto" href="https://www.sundevilauto.com/"/>
              <Portfolio bgImage={dusa} name="DeferoUSA" href="https://www.deferousa.com/"/>
              <Portfolio bgImage={bm} name="Billy Margot" href="https://offers.billyandmargot.com/lp/signupoffers/"/>
              <Portfolio bgImage={sns} name="Smooth N Shine" href="https://www.smoothnshine.com/"/>

            </div>
            <h3 className="third-header">And More! Contact me if you want to see more!</h3>

          </div>


          <Playarea/>

        </Layout>
  		);
  	}
  }

export default IndexPage
