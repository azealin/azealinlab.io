module.exports = {
  pathPrefix: `/`,
  siteMetadata: {
    title: `Ashley Yang Personal Website`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`

  ],
}
